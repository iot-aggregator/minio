#!/bin/bash

############################################################
# Help                                                     #
############################################################
Help()
{
   # Display Help
   echo "Add specified bucker to MinIO server and make it publically available"
   echo
   echo "Syntax: configure-bucket [-a|-h|-k|-s|-b|-d|-l]"
   echo "options:"
   echo "a    MinIO server address."
   echo "h    Print this Help."
   echo "k    Access key for MinIO server."
   echo "s    Secret key for MinIO server."
   echo "b    Bucket name."   
   echo "d    Download mc commands script and remove it after completion."
   echo "l    Use locally installed mc commands."
   echo
}

Address=""
AccessKey=""
Secret=""
BucketName=""
Download=false
Local=false

while getopts ":ha:k:s:n:b:dl:" option; do
    case $option in
    h) # display Help
        Help
        exit;;
    a)
        Address=$OPTARG;;
    k)
        AccessKey=$OPTARG;;
    s)
        Secret=$OPTARG;;	
    b)
        BucketName=$OPTARG;;	  
    d)
        Download=true;;	  
    l)
        Local=true;;	  		  
    esac
done

if [[ -z "$Address" ]]; then
    echo "Address for MinIO server is required." 1>&2
    exit 1
fi

if [[ -z "$AccessKey" ]]; then
    echo "Access key for MinIO server is required." 1>&2
    exit 1
fi

if [[ -z "$Secret" ]]; then
    echo "Secret for MinIO server is required." 1>&2
    exit 1
fi

if [ "$Download" = true ]
then
    wget https://dl.min.io/client/mc/release/linux-amd64/mc
    chmod +x mc
    ./mc alias set s3 $Address $AccessKey $Secret
    ./mc mb s3/$BucketName
    ./mc policy set public s3/$BucketName
    rm mc
else
    if [ "$Local" = true ]
    then
        ./mc alias set s3 $Address $AccessKey $Secret
        ./mc mb s3/$BucketName
        ./mc policy set public s3/$BucketName
    else
        mc alias set s3 $Address $AccessKey $Secret
        mc mb s3/$BucketName
        mc policy set public s3/$BucketName
    fi
fi



