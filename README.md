# MinIO service

## Setup
To setup MinIO service after it's initialization, use `configure-bucket.sh`. For example, for bucket "photos" on server http://minio.bucket with keys `minioadmin:minioadmin` use:
`./configure-bucket.sh -a htp://minio.bucket -k minioadmin -s minioadmin -b photos -d`

Shell script will create bucket on server and set its policy to public, so anyone can view images in this bucket without generating any access tokens.