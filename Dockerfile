FROM minio/minio

CMD ["minio", "server", "--address", ":9000", "/data"]